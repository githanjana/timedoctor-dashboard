export default {

  title: {
    display: false,
  },
  legend: {
    display: false,
  },
  tooltips: {
    caretSize: 0,
    xPadding: 12,
    yPadding: 12,
    bodyFontSize: 11,
    footerFontSize: 11,
    multiKeyBackground: 'transparent',
    position: 'nearest',
    callbacks: {
      label(toolTipItem, data) {
        return ` ${data.datasets[toolTipItem.datasetIndex].label}`;
      },
      footer(toolTipItem) {
        return `     ${Math.abs(toolTipItem[0].yLabel)}%`;
      },
    },
  },
  scales: {
    xAxes: [{
      stacked: true,
      barPercentage: 0.7,
      gridLines: {
        display: false,
        drawBorder: false,
      },
      ticks: {
        display: false,
      },
    }],
    yAxes: [{
      gridLines: {
        display: true,
        color: '#d9d9db',
        drawBorder: false,
        zeroLineWidth: 1,
        zeroLineBorderDash: [4, 4],
        zeroLineColor: '#d9d9db',
        borderDash: [4, 4],
      },
      ticks: {
        min: -100,
        max: 100,
        padding: 15,
        fontColor: '#8a9faa',
        fontSize: 11,
        maxTicksLimit: 4,
        callback(value) {
          return `${Math.abs(value)}%`;
        },
      },
    }],
  },
};

