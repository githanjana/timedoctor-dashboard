import React from 'react';
import PropTypes from 'prop-types';

const styles = {
  indicator: {
    width: 8,
    height: 8,
  },
};

const ChartLegendItem = props => (
  <div className="d-flex align-items-center chart-widget-legend-item">
    <div
      style={{
        backgroundColor: props.color,
        borderRadius: props.round ? 4 : 0,
        ...styles.indicator,
      }}
    />
    <span style={{ fontSize: 11, color: '#8a9faa', paddingLeft: 4 }}>{props.label}</span>
  </div>
);

ChartLegendItem.propTypes = {
  label: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  round: PropTypes.bool,
};

ChartLegendItem.defaultProps = {
  round: false,
};

export default ChartLegendItem;
