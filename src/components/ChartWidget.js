import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ChartWidget extends Component {
  constructor() {
    super();
    this.state = {
      showOptions: false,
    };
    this.toggleOptions = this.toggleOptions.bind(this);
    this.renderOptionsView = this.renderOptionsView.bind(this);
  }

  toggleOptions() {
    this.setState({ showOptions: !this.state.showOptions });
  }

  renderOptionsView() {
    return (
      <form className="p-3 chart-widget-options-form">
        <div className="form-group">
          <label htmlFor="usersinput">Users</label>
          <select className="form-control" id="usersinput">
            <option>5 People Selected</option>
            <option>4 People Selected</option>
            <option>3 People Selected</option>
            <option>2 People Selected</option>
            <option>1 Person Selected</option>
          </select>
          <small id="emailHelp" className="form-text text-muted">
            Note: Please select min 2 people for max 5 users to compare.
          </small>
        </div>
        <div className="form-group">
          <label htmlFor="datesinput">Date</label>
          <select className="form-control" id="datesinput">
            <option>Last 30 Days</option>
            <option>Last 7 Days</option>
          </select>
        </div>
        <div className="d-flex justify-content-end align-items-end">
          <div className="chart-widget-button-cancel" />
          <div className="chart-widget-button-save" />
        </div>
      </form>
    );
  }

  render() {
    return (
      <div className="chart-widget">
        <div className="d-flex justify-content-between chart-widget-header">
          <div className="d-flex flex-column justify-content-start ">
            <span >{this.props.smallHeaderText}</span>
            <h5>{this.props.bigHeaderText}</h5>
          </div>
          <div className="d-flex align-items-center">
            <div className="dropdown">
              <div className="dropdown-toggle" data-toggle="dropdown">Last 7 days</div>
              <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <button className="dropdown-item" type="button">Monthly</button>
                <button className="dropdown-item" type="button">Daily</button>
              </div>
            </div>
            <div
              role="presentation"
              onClick={this.toggleOptions}
              className="chart-widget-option-button"
            />
          </div>
        </div>
        {
          (() => {
            if (this.state.showOptions) {
              return this.renderOptionsView();
            }
            return this.props.children;
          })()
        }
        <div className="d-flex justify-content-between align-items-center chart-widget-footer">
          <span>10 Users Selected</span>
          <span>See Screenshot Report</span>
        </div>
      </div>
    );
  }
}

ChartWidget.propTypes = {
  smallHeaderText: PropTypes.string.isRequired,
  bigHeaderText: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default ChartWidget;
