import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';
import PropTypes from 'prop-types';

import ChartWidget from './ChartWidget';
import ChartLegendItem from './ChartLegendItem';

import chartOptions from '../config/lineChartOptions';

class LineChartWidget extends Component {
  render() {
    const { chartData } = this.props;
    const legendItems = chartData.datasets.map((item, index) => (
      <ChartLegendItem
        key={index}
        label={item.label}
        color={item.borderColor}
        round
      />
    ));
    return (
      <ChartWidget
        smallHeaderText={this.props.smallHeaderText}
        bigHeaderText={this.props.bigHeaderText}
        dropdownCallback={this.handleDropDown}
      >
        <div style={{ maxWidth: 460 }}>
          <div style={{ display: 'flex', padding: 6, marginLeft: 40 }}>
            { legendItems }
          </div>
          <Line
            data={chartData}
            width={460}
            height={192}
            options={chartOptions}
          />
        </div>
      </ChartWidget>
    );
  }
}

LineChartWidget.propTypes = {
  smallHeaderText: PropTypes.string.isRequired,
  bigHeaderText: PropTypes.string.isRequired,
  chartData: PropTypes.shape({}).isRequired,
};

export default LineChartWidget;
