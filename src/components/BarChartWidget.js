import React, { Component } from 'react';
import { Bar } from 'react-chartjs-2';
import PropTypes from 'prop-types';

import avatar from '../img/avatar1.png';
import ChartLegendItem from './ChartLegendItem';
import ChartWidget from './ChartWidget';

import chartOptions from '../config/barChartOptions'; // Chart Options object for ChartJS

class BarChartWidget extends Component {
  constructor() {
    super();
    this.handleDropDown = this.handleDropDown.bind(this);
  }

  handleDropDown() {
    this.setState({});
  }

  render() {
    const { chartData } = this.props;
    const legendItems = chartData.datasets.map((item, index) => (
      <ChartLegendItem
        key={index}
        label={item.label}
        color={item.backgroundColor}
      />
    ));
    const avatarTicks = chartData.labels.map((val, index) => (
      <img key={index} src={avatar} alt="" />
    ));
    return (
      <ChartWidget
        smallHeaderText={this.props.smallHeaderText}
        bigHeaderText={this.props.bigHeaderText}
        dropdownCallback={this.handleDropDown}
      >
        <div style={{ maxWidth: 470 }}>
          <div style={{ display: 'flex', padding: 6, marginLeft: 40 }}>
            { legendItems }
          </div>
          <Bar
            data={chartData}
            width={460}
            height={165}
            options={chartOptions}
          />
          <div className="d-flex justify-content-around chart-widget-avatar-ticks">
            { avatarTicks }
          </div>
        </div>
      </ChartWidget>
    );
  }
}

BarChartWidget.propTypes = {
  smallHeaderText: PropTypes.string.isRequired,
  bigHeaderText: PropTypes.string.isRequired,
  chartData: PropTypes.shape({}).isRequired,
};

export default BarChartWidget;
