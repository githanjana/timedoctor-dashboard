import React, { Component } from 'react';
import BarChartWidget from './components/BarChartWidget';
import LineChartWidget from './components/LineChartWidget';

import chartData from './data';

class App extends Component {
  constructor() {
    super();
    this.state = {
      chartData1: {},
      chartData2: {},
    };
  }

  componentWillMount() {
    this.getChartData();
  }

  getChartData() {
    /**
     * Fetching the data an setting it to state.
     * In this case it's just setting the data from a local file.
     * If Redux is used, this can be an action dispatch.
     */
    this.setState(chartData);
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="p-4">
          <h3>Timedoctor Dashboard</h3>
        </div>
        <div className="d-flex flex-wrap">
          <BarChartWidget
            chartData={this.state.chartData1}
            bigHeaderText="Low Keyboard & Mouse Activity"
            smallHeaderText="Users with"
          />
          <LineChartWidget
            chartData={this.state.chartData2}
            bigHeaderText="Time Tracked"
            smallHeaderText="Comparing teams"
          />
        </div>
      </div>
    );
  }
}

export default App;
