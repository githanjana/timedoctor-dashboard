## a. How the task was executed

I started with a new react project with ChartJS 2 framework and got the basic chart views done by tweaking the chart options. Then marked down the rest of the UIs using HTML and CSS(SCSS).

## b. Libraries used.

	-ReactJS
	-ChartJS
	-Bootstrap 4 framework

## c. Issues faced.

Mainly configuring ChartJS to get the desired results. while it being a nice customizable library, there are some deadends such as the customizablility of the chart legend  where I had to come up with work arounds.